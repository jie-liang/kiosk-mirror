package kiosk;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Jie Liang
 * ID#: 109371227
 * Homework #5
 * Thursday: R06
 * Kevin Flyangolts
 * Grading TA: Zheyuan (Jeffrey) Gao
 * @author jieliang
 */

public class Tree {
	
	private TreeNode root;
	private TreeNode[] data;
	private int top;
	private final int SELECTION_CAPACITY = 20;
	private StringBuilder sb;
	
	/**
	 * 
	 */
	public Tree()
	{
		root = null;
		top = -1;
		data = new TreeNode[SELECTION_CAPACITY];
		sb = new StringBuilder();
		sb.append(String.format("%-20s%-50s%-10s", "Dining", "Selection", "Price"));
		sb.append("\n-----------------------------------------------------------------------------------------\n");
	}
	
	/**
	 * @param name
	 * @param node
	 * @return
	 * @throws NoSuchChildException
	 */
	public TreeNode findNode(String name, TreeNode node) throws NoSuchChildException
	{
		if(node != null)
            if (node.getName().equals(name))
                return node;
            else {
            	TreeNode temp = findNode(name, node.getNthChildren(1));
            	int n = 1;
            	while(temp == null && n < 10)
            	{
            		temp = findNode(name, node.getNthChildren(++n));
            	}
                return temp;
            }
		else
		{
			return null;
		}
	}
	
	/**
	 * @param name
	 * @param selection
	 * @param message
	 * @param parentName
	 * @return
	 * @throws NoSuchChildException
	 */
	public boolean addNode(String name, String selection, String message, String parentName) throws NoSuchChildException
	{
		TreeNode newNode = new TreeNode();
		newNode.setName(name);
		newNode.setSelection(selection);
		newNode.setMessage(message);

		if(root != null)
		{
			TreeNode node = findNode(parentName, root);
			if(node != null)
			{
				if(node.numChildren() < TreeNode.CHILDREN_CAPACITY)
				{
					node.setNthChildren(node.numChildren() + 1, newNode);
					return true;
				}
				else
					return false;
			}
			else
			{
				return false;
			}
		}
		else
		{
			root = newNode;
			return true;
		}
	}
	
	/**
	 * @throws NoSuchChildException
	 */
	public void printMenu() throws NoSuchChildException
	{
		for(int i = 1; i <= TreeNode.CHILDREN_CAPACITY; i++)
		{
			TreeNode child = root.getNthChildren(i);
			if(child != null)
			{
				TreeNode cursor = child;
				for(int j = 1; j <= TreeNode.CHILDREN_CAPACITY; j++)
				{
					if(cursor.getNthChildren(j) != null)
					{
						cursor = cursor.getNthChildren(j);
					}
				}
			}
		}
		System.out.println(sb.toString());
	}
	
	/**
	 * @param node
	 * @param parentInfo
	 */
	public void print(TreeNode node, String parentInfo)
	{
		//preorder
		if(node != null)
		{
            if (node.isLeaf())
            	sb.append(String.format("%-20s%-10s",parentInfo, node.getMessage()));
            else {
            	TreeNode temp = print(node.getNthChildren(1), parentInfo);
            	int n = 1;
            	while(temp == null && n < 10)
            	{
            		temp = print(node.getNthChildren(++n), parentInfo);
            	}
            }
		}
	}
	
	/**
	 * @param fileName
	 * @throws IOException
	 * @throws NoSuchChildException
	 */
	public void beginSession(String fileName) throws IOException, NoSuchChildException
	{
		FileInputStream fis = new FileInputStream(fileName); 
		InputStreamReader inStream = new InputStreamReader(fis);
		BufferedReader reader = new BufferedReader(inStream);

		String data = reader.readLine();
		int children = 0;
		String parentName = null;
		if(data != null)
		{
			String name = data;
			data = reader.readLine();
			String selection = data;
			data = reader.readLine();
			String message = data;
			addNode(name, selection, message, parentName);
		}
		while((data = reader.readLine()) != null)
		{
			String information = data;
			StringTokenizer j = new StringTokenizer(information);
			while(j.hasMoreTokens())
			{
				parentName = j.nextToken();
				children = Integer.parseInt(j.nextToken());
			}
			for(int i = 1; i <= children; i++)
			{
				data = reader.readLine();
				String name = data;
				data = reader.readLine();
				String selection = data;
				data = reader.readLine();
				String message = data;
				addNode(name, selection, message, parentName);
			}
		}
	}
	
	/**
	 * @param name
	 * @return
	 * @throws NoSuchChildException
	 */
	public TreeNode findNode(String name) throws NoSuchChildException
	{
		return findNode(name, root);
	}
	
	/**
	 * @throws NoSuchChildException
	 * @throws FullStackException
	 */
	public void getSelection() throws NoSuchChildException, FullStackException
	{
		TreeNode cursor = root;
		Scanner user_input = new Scanner(System.in);

		while(!cursor.isLeaf())
		{
			System.out.println(cursor.getMessage());
			push(cursor);
			for(int i = 1; i <= cursor.numChildren(); i++)
			{
				System.out.println(i + " " + cursor.getNthChildren(i).getSelection());
			}
			System.out.println("99 Go back");
			System.out.println("0 Exit Session");
			System.out.print("Choice: ");
			
			try{
				int selection = Integer.parseInt(user_input.next());
				System.out.println();
				if(selection == 0)
				{
					break;
				}
				else if(selection == 99)
				{
					if(top > 0)
					{
						pop();
						cursor = pop();
					}
					
				}
				else if(0 < selection && selection <= cursor.numChildren())
				{
					cursor = cursor.getNthChildren(selection);
				}
				else
				{
					System.out.println("No such choice!");
				}
			}
			catch(NumberFormatException e){
				System.out.println("Invalid Input!");
			}

		}
		push(cursor);
		if(cursor.isLeaf())
		{
			String order = data[1].getSelection();
			TreeNode n = pop();
			String price = n.getMessage();
			System.out.print("The order at " + order + ": ");
			for(int i = 2; i < data.length; i++)
			{
				if(data[i] != null)
				{
					System.out.print(data[i].getSelection() + ", ");
				}
			}
			System.out.println(n.getSelection() + " has been sent to the kitchen. Total amount would be " + price);
		}
		data = new TreeNode[SELECTION_CAPACITY];
		top = -1;
	}
	
	private void push(TreeNode node) throws FullStackException
	{
		if(top == SELECTION_CAPACITY -1)
			throw new FullStackException();
		top++;
		data[top] = node;
	}
	/**
	 * @return
	 */
	private TreeNode pop()
	{
		TreeNode answer;
		if(top == -1)
			throw new EmptyStackException();
		answer = data[top];
		data[top] = null;
		top--;
		return answer;
	}
}
